package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
        /* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

        while (true) {
            System.out.println ("Let's play round " + roundCounter);

            String humanChoice = userChoice("Your choice (Rock/Paper/Scissors)?", rpsChoices);
            
            String cpChoice = computerChoice();


            if (roundWinner(humanChoice, cpChoice)) {
                humanScore ++;
                System.out.println("Human chose " + humanChoice + ", computer chose " + cpChoice + ". Human wins!");
            }
            else if (roundWinner(cpChoice, humanChoice)) {
                computerScore++;
                System.out.println("Human chose " + humanChoice + ", computer chose " + cpChoice + ". Computer wins!");
            }
            else {
                System.out.println("Human chose " + humanChoice + ", computer chose " + cpChoice + ". It's a tie!");
            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String newRound = userChoice("Do you wish to continue playing? (y/n)?", Arrays.asList("y", "n"));
            if(newRound.equals("n")) {
                System.out.println ("Bye bye :)");
                break;
            }


            roundCounter++;
        }
    }

    public String userChoice(String prompt, List<String> validateInput) {
        String userChoice;
        while (true) {
            userChoice = readInput(prompt);
            if (validInput(userChoice, validateInput)) {
                break;
            }
            else {
                System.out.println("I do not understand " + userChoice + ". Could you try again?");
            }
        }
        return userChoice;
    }

    public String computerChoice() {
        Random random = new Random();
        int randomIndex = random.nextInt(rpsChoices.size());
        return rpsChoices.get(randomIndex);
    }

    public boolean roundWinner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");            
        }
        if (choice1.equals("rock")) {
            return choice2.equals("scissors");            
        }
        else {
            return choice2.equals("paper");
        }
    }

    public boolean validInput(String input, List<String> validInput) {
        return validInput.contains(input);
    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
